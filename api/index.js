import { join, dirname } from 'path'
import { Low, JSONFile } from 'lowdb'
import { fileURLToPath } from 'url'
import lodash from 'lodash'
import Fastify from 'fastify'
import fastifyCors from '@fastify/cors'

const fastify = Fastify({
  logger: true
})

class LowWithLodash extends Low {
    chain = lodash.chain(this).get('data')
}

const __dirname = dirname(fileURLToPath(import.meta.url));

const file = join(__dirname, 'db.json')

const adapter = new JSONFile(file)
const db = new LowWithLodash(adapter)

await db.read()

db.data = db.data || { tasks: [] }

const boardTasks = db.chain.get('tasks').filter({ board_id: 1 }).value()
console.log(boardTasks)

fastify.register(fastifyCors, {
  origin: true
})

fastify.get('/tasks', async (req, res) => {
  return db.chain.get('tasks').value().map(({ id, name }) => ({ id, name }));
})

fastify.get('/tasks/:id', async (req, res) => {
  const id = parseInt(req.params.id);
  console.log(id)
  console.log(db.chain.get('tasks').find({ id }).value());
  return db.chain.get('tasks').find({ id }).value();
})

fastify.post('/tasks', async (req, res) => {
  const id = db.chain.get('tasks').value().length
  const body = JSON.parse(req.body);
  db.chain.get('tasks').unshift({ ...body, id }).value();
  await db.write();
  res.code(204);
})


const start = async () => {
  try {
    await fastify.listen(4030)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()