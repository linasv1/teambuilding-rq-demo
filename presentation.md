---
marp: true
---

# State management

### Server state vs Client state

---

# How do we tend to manage state in the front-end?

## Global State

```
{
  navbarOpen: true,
  theme: 'dark',
  notifications: [...],
  users: [...],
  posts: [...],
  currentPost: {...},
  sidebarOpen: false,
}
```

---

# Global State

| Client State      | Server State            |
| ----------------- | ----------------------- |
| Non-persistent    | Remotely persisted      |
| Synchronous       | Asynchronous            |
| Client-owned      | Shared ownership        |
| Always up to date | Potentially out of date |

---

# Server State challenges

- Caching
- Deduping requests
- Background updates
- Outdated requests
- Paginations, incremental fetching

---

# The Problem

### The data that we keep in global state is of two different origins and thus have different features

---

# Potential Solution?

### Following the idea of Separation of Concerns, we could start treating Client and Server States as different entities, rather than putting it all in one place

---

# Global State

```diff
{
  navbarOpen: true,
  theme: 'dark',
  notifications: [...],
  users: [...],
  posts: [...],
  currentPost: {...},
  sidebarOpen: false,
}
```

---

# Client State

```diff
{
  navbarOpen: true,
  theme: 'dark',
  sidebarOpen: false,
}
```

---

# So what do we do with the server state then?

---

# React Query

### There are some alternatives too

---

## React Query is..

### a library for fetching, caching, synchronizing and updating server state

#

## React Query isn't..

### a client state management library

---

# React Query Concepts

- Queries
- Mutations

---

# Queries

```
const query = useQuery('tasks', getTasks);

query.data // [...]
```

---

# Mutations

```
const mutation = useMutation(createTask, {
  onSuccess: (newTask) => {
    queryClient.invalidateQueries('tasks')
  }
})

mutation.mutate({
  name
});
```
