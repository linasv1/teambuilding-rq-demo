import React from 'react';

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import { Tasks } from './components/Tasks';
import './App.css';
import { QueryClient,
  QueryClientProvider,
} from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import { Task } from './components/Task';
import { QueryStatus } from './components/QueryStatus';

const queryClient = new QueryClient()

function App() {
  return (
    <BrowserRouter>
      <QueryClientProvider client={queryClient}>
      <QueryStatus />
      <Routes>
          <Route index element={<Tasks />} />
          <Route path="/tasks/:id" element={<Task />} />
      </Routes>
      <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </BrowserRouter>
  );
}

export default App;
