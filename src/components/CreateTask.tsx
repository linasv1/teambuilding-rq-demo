import { TextField } from '@mui/material';
import React, { useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { createTask } from '../data/api';

export const CreateTask = () => {

  const queryClient = useQueryClient()
  const mutation = useMutation(createTask, {
    // // OPTIMISTIC UPDATES
    // onMutate: (newTask) => {

    //   // Cancel current queries for 'tasks' key, so that optimistic updates aren't overwritten

    //   // Save a snapshot of current query data in case we need to reset to previous state

    //   // Add the task which is being created to the query data

    //   // Return previously saved snapshot so we can use it later

    // },
    // onError: (err, newTask, context: any ) => {
    //   // In case of an error, revert to previous state
    // },
    onSuccess: () => {
      queryClient.invalidateQueries('tasks')
    }
  })

  const [name, setName] = useState('');

  const handleChange = (e: any) => {
    setName(e.target.value)
  }

  const handleSubmit = (e: any) => {
    e.preventDefault();
    mutation.mutate({
      name
    });
  }

  return (
    <form onSubmit={handleSubmit}>
    <TextField variant="outlined" placeholder="Create a new task" size="small" onChange={handleChange}/>
    </form>
  )
}