import { Chip } from '@mui/material';
import React from 'react';
import { useIsFetching, useIsMutating } from 'react-query';

export const QueryStatus = () => {
  const isMutating = useIsMutating()
  const isFetching = useIsFetching()

  return (
    <>
    { isMutating ? <Chip label="Mutating..." color="primary" /> : null}
    { isFetching ? <Chip label="Fetching..." color="primary" /> : null}
    </>
  )
}