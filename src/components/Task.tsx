import { Typography } from '@mui/material';
import React from 'react';
import { useQuery } from 'react-query';
import { useParams } from 'react-router';
import { getTask } from '../data/api';
import { TaskType } from '../data/types';

export const Task = () => {
  const { id } = useParams()
  const query = useQuery<TaskType>(['task', id], getTask);


  return query.data ? (
    <>
    <Typography variant="h2">{query.data.name}</Typography>
    <Typography variant="body1">{query.data.description}</Typography>
    </>) : <></>
}