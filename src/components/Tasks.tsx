import React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import { Divider, ListItemButton, ListItemText } from '@mui/material';
import { useQuery } from 'react-query';
import { getTasks } from '../data/api';
import { CreateTask } from './CreateTask';
import { useNavigate } from 'react-router-dom';
import { TaskType } from '../data/types';

export const Tasks = () => {

  const query = useQuery<TaskType[]>('tasks', getTasks);
  const navigate = useNavigate();

  return (
    <>
    <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
    <ListItem>
      <CreateTask />
    </ListItem>
  <Divider />
    {query.data?.map((task: any) => (
      <ListItem>
      <ListItemButton onClick={() => navigate(`/tasks/${task.id}`)}>
      <ListItemText primary={task.name} />
      </ListItemButton>
    </ListItem>
    ))}
  </List>
  </>
)};