export const getTasks = async () => {
  const res = await fetch('http://localhost:4030/tasks');
  return await res.json();
}

export const getTask = async ({ queryKey }: any) => {
  const [_, id] = queryKey;
  const res = await fetch(`http://localhost:4030/tasks/${id}`);
  return await res.json();
} 

export const createTask = async (payload: any) => {
  await fetch('http://localhost:4030/tasks', { method: 'POST', body: JSON.stringify(payload) });
  return;
}